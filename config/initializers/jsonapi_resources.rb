# frozen_string_literal: true

JSONAPI.configure do |config|
  DEFAULT_PAGE_SIZE = 5
  config.default_page_size = DEFAULT_PAGE_SIZE
  config.maximum_page_size = DEFAULT_PAGE_SIZE

  config.top_level_meta_include_page_count = true

  config.default_processor_klass = JSONAPI::Authorization::AuthorizingProcessor
  config.exception_class_whitelist = [Pundit::NotAuthorizedError]
end

JSONAPI::Authorization.configure do |config|
  config.pundit_user = :current_user
end
