# frozen_string_literal: true

Rails.application.routes.draw do
  jsonapi_resources :users
  jsonapi_resources :rentals

  post '/login', to: 'sessions#create'
end
