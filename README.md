## Backend
### Installation

You will need *Docker*, `docker-compose` and `docker-sync` in order to run the backend application.

```sh
$ docker-compose build # build the app
$ docker-sync start # start the sync volume
$ docker-compose run web rails db:create # create the database
$ docker-compose run web rails db:migrate # run the migrations
$ docker-compose run web rails db:migrate RAILS_ENV=test # run the migrations for the test db
$ docker-compose run web rails db:seed # seed the database
```
### Running
```sh
$ docker-compose up # run the app container
```

### Running the test suite
```sh
$ docker-compose run web rspec spec
```

### Running the console
```sh
$ docker-compose run web rails console
```

## Frontend
### Installation

Make sure you have yarn installed. The version of *node* used is `9.10.1`, so make sure you have that.

```sh
$ cd ./client
$ yarn install # get the dependencies
```

### Running

```sh
$ yarn start # start the app
```
