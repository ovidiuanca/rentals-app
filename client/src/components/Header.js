import { NavLink } from 'react-router-dom'
import React, { Component, Fragment } from 'react'
import autobind from 'autobind-decorator'
import classnames from 'classnames'

import * as actions from '../actions'

@autobind
export default class Header extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    const {
      user,
      setLoginModalVisibility,
      setSignupModalVisibility,
      logout,
    } = this.props

    return (
      <header id="header">
        <div className="logo">
          <NavLink to="/" exact activeClassName="active">
            Rentals app
          </NavLink>
        </div>
        <nav className="nav">
          <ul>
            <li>
              {
                user ?
                  <div>
                    <NavLink className="nav-link" to="/rentals" exact activeClassName="active">
                      Rentals
                    </NavLink>
                    {user.role === 'admin' &&
                      <NavLink className="nav-link" to="/users" exact activeClassName="active">
                        Users
                      </NavLink>
                    }
                    <Fragment>
                      <span className="nav-link no-link">
                        {`${user.email}`}
                      </span>
                      <span className="nav-link no-link">
                        {`${user.role}`}
                      </span>
                      <span
                        className="nav-link btn btn-sm btn-outline"
                        onClick={logout}
                      >
                        Logout
                      </span>
                    </Fragment>
                  </div>
                  :
                  <Fragment>
                    <span
                      className="nav-link btn btn-sm btn-outline"
                      onClick={setSignupModalVisibility.bind(this, true)}
                    >Sign up</span>
                    <span
                      className="nav-link btn btn-sm"
                      onClick={setLoginModalVisibility.bind(this, true)}
                    >Login</span>
                  </Fragment>
              }
            </li>
          </ul>
        </nav>
      </header>
    )
  }
}

