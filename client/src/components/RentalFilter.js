import React, { Component } from 'react'
import autobind from 'autobind-decorator'
import { TextField, Button } from '@material-ui/core'
import { connect } from 'react-redux'

import * as actions from '../actions'

@connect(state => ({
  maxPrice: state.rental.filters.maxPrice,
  maxArea: state.rental.filters.maxArea,
  maxRooms: state.rental.filters.maxRooms,
}), actions)
@autobind
export default class RentalFilter extends Component {
  constructor(props) {
    super(props)

    this.state = {
      maxPrice: props.maxPrice || null,
      maxRooms: props.maxRooms || null,
      maxArea: props.maxArea || null,
    }
  }

  handlePriceChange(e) {
    const value = parseInt(e.target.value)

    this.setState({ maxPrice: value })
  }

  handleRoomsChange(e) {
    const value = parseInt(e.target.value)

    this.setState({ maxRooms: value })
  }

  handleAreaChange(e) {
    const value = parseInt(e.target.value)

    this.setState({ maxArea: value })
  }

  handleApplyFilter() {
    this.props.changeRentalsFilters(this.state)
  }

  render() {
    const { maxPrice, maxRooms, maxArea } = this.state
    return(
      <form className="filterForm" noValidate autoComplete="off">
        <TextField id="price" className="textField" defaultValue={maxPrice} label="Price" variant="outlined" type="number" size="small" onChange={this.handlePriceChange} />
        <TextField id="rooms" className="textField" defaultValue={maxRooms} label="Rooms" variant="outlined" type="number" size="small" onChange={this.handleRoomsChange} />
        <TextField id="area" className="textField" defaultValue={maxArea} label="Size" variant="outlined" type="number" size="small" onChange={this.handleAreaChange} />
        <Button variant="contained" className="filterButton" onClick={this.handleApplyFilter}>
          Apply
        </Button>
      </form>
    )
  }
}
