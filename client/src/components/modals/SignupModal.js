import React from 'react'
import autobind from 'autobind-decorator'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { change } from 'redux-form'

import * as actions from '../../actions'

import SignupForm from '../forms/SignupForm'
import swal from 'sweetalert2'

@connect(state => ({
}), dispatch => {
  return bindActionCreators({
    ...actions,
  }, dispatch)
})
@autobind
export default class SignupModal extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }


  handleSignupSubmit(values) {
    const {
      signUp,
    } = this.props

    signUp({
      ...values,
    })
    .then(() => {
      swal('Account created successfully.')
    })
    .catch((err) => {
      if (err && err.body && err.body.errors) {
        swal('ERROR', err.body.errors[0].detail, 'error')
      } else {
        swal('ERROR', 'Something went wrong. Please try again later!', 'error')
      }
    })
  }

  render() {
    const {
      setLoginModalVisibility,
      setSignupModalVisibility,
    } = this.props

    return (
      <div className="modal-container">
        <div className="title">
          <p className="dis-flex primary bold mb-20 uppercase">
            Sign up
          </p>
        </div>
        <SignupForm onSubmit={this.handleSignupSubmit}/>
        <div className="mt-30">
          <p className="center">
            Already signed up ?&nbsp;
            <span className="link" onClick={() => {
              setLoginModalVisibility(true)
              setSignupModalVisibility(false)
            }}>Log In</span>
          </p>
        </div>
      </div>
    )
  }
}
