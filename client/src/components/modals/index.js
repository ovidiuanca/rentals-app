import React from 'react'
import autobind from 'autobind-decorator'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Modal from 'react-modal'

import * as actions from '../../actions'

import LoginModal from './LoginModal'
import SignupModal from './SignupModal'

@connect(state => ({
  modal: state.modal,
}), actions)
@autobind
export default class Modals extends React.Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    const {
      modal,
      setLoginModalVisibility,
      setSignupModalVisibility,
    } = this.props


    const regularClassName = {
      base: 'react-modal-regular',
      afterOpen: 'react-modal-regular_after-open',
      beforeClose: 'react-modal-regular_before-close',
    }

    const overlayClassName = {
      base: 'react-modal-overlay',
      afterOpen: 'react-modal-overlay_after-open',
      beforeClose: 'react-modal-overlay_before-close',
    }

    return (
      <div>
        <Modal
          isOpen={modal.loginModalVisible}
          onRequestClose={setLoginModalVisibility.bind(this, false)}
          className={regularClassName}
          overlayClassName={overlayClassName}
          ariaHideApp={false}
          >
          <LoginModal />
        </Modal>
        <Modal
          isOpen={modal.signupModalVisible}
          onRequestClose={setSignupModalVisibility.bind(this, false)}
          className={regularClassName}
          overlayClassName={overlayClassName}
          ariaHideApp={false}
          >
          <SignupModal />
        </Modal>
      </div>
    )
  }
}
