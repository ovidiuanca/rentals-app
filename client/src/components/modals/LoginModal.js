import React from 'react'
import autobind from 'autobind-decorator'
import { connect } from 'react-redux'

import * as actions from '../../actions'

import LoginForm from '../forms/LoginForm'
import swal from 'sweetalert2'

@connect(state => ({
  loginPending: state.user.loginPending,
}), actions)
@autobind
export default class LoginModal extends React.Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  handleLoginSubmit(values) {
    const {
      login,
    } = this.props

    login(values)
    .catch((err) => {
      if (err.data && err.data.error) {
        swal('ERROR', err.data.error, 'error')
      } else {
        swal('ERROR', 'Something went wrong. Please try again later!', 'error')
      }
    })
  }

  render() {
    const {
      setLoginModalVisibility,
      setSignupModalVisibility,
      loginPending,
    } = this.props

    return (
      <div className="modal-container">
        <div className="title">
          <p className="dis-flex primary bold mb-20 uppercase">
            LOGIN
          </p>
        </div>
        <LoginForm
          onSubmit={this.handleLoginSubmit}
          loginPending={loginPending}
        />
        <div className="mt-30">
          <p className="center">
            Don’t have account ? Please&nbsp;
            <span
              className="link"
              onClick={() => {
                setLoginModalVisibility(false)
                setSignupModalVisibility(true)
              }}>Sign Up</span>
          </p>
        </div>
      </div>
    )
  }
}
