import React from 'react'
import { Field, reduxForm } from 'redux-form'

import { Button } from '@material-ui/core'

import {
  renderTextField,
  renderSelectField,
} from './fields'

import * as utils from '../../utils'

const validate = (values, props) => {
  const errors = {}

  if (!values.email || !utils.isEmailValid(values.email)) {
    errors.description = 'Please input a valid email'
  }

  if (values.password && values.password.length < 4) {
    errors.password = 'Password must be at least 4 characters long'
  }

  if (values['password-confirmation'] !== values.password) {
    errors['password-confirmation'] = 'Passoword confirmation is wrong'
  }

  return errors
}

const roleOptions = [
  {
    label: 'Admin',
    value: 'admin',
  },
  {
    label: 'Realtor',
    value: 'realtor',
  },
  {
    label: 'Client',
    value: 'client',
  },
]

let EditUserForm = ({ handleSubmit, updatePending }) => (
  <form onSubmit={handleSubmit}>
    <label>Email</label>
    <Field name="email" component={renderTextField} type="text" placeholder="Email" label="Email" />
    <label>Password</label>
    <Field name="password" component={renderTextField} type="password" placeholder="Password" />
    <label>Password confirmation</label>
    <Field name="password-confirmation" component={renderTextField} type="password" placeholder="Confirm Password"/>
    <Field name="role" className="roleSelect" component={renderSelectField} label="Role">
      { roleOptions.map((role) =>
        <option key={role.value} value={role.value}>{role.label}</option>
      )}
    </Field>
    <button
      className="btn w-100"
      type="submit"
      disabled={updatePending}
    >Submit</button>
  </form>
)

export default reduxForm({
  form: 'editUser',
  validate,
  fields: ['email', 'role'],
})(EditUserForm)
