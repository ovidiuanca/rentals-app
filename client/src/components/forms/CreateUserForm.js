import React from 'react'
import { Field, reduxForm } from 'redux-form'

import {
  renderTextField,
  renderTextareaField,
} from './fields'

import * as utils from '../../utils'

const validate = (values, props) => {
  const errors = {}

  if (!values.email) {
    errors.email = 'Email is required'
  } else if (values.email && !utils.isEmailValid(values.email)) {
    errors.email = 'Email is not valid'
  }

  if (!values.password) {
    errors.password = 'Password is required'
  } else if (values.password && values.password.length < 4) {
    errors.password = 'Password must be at least 4 characters long'
  }

  if (!values['password-confirmation']) {
    errors['password-confirmation'] = 'Password confirmation is required'
  } else if (values['password-confirmation'] !== values.password) {
    errors['password-confirmation'] = 'Passoword confirmation is wrong'
  }

  return errors
}

let CreateRentalForm = ({ handleSubmit, createPending }) => (
  <form onSubmit={handleSubmit}>
    <label>Email</label>
    <Field name="email" component={renderTextField} type="text" placeholder="test@example.com" />
    <label>Password</label>
    <Field name="password" component={renderTextField} type="password" placeholder="Password" />
    <label>Password confirmation</label>
    <Field name="password-confirmation" component={renderTextField} type="password" placeholder="Confirm Password"/>
    <div className="right mt-30">
      <button
        className="btn w-100"
        type="submit"
        disabled={createPending}
      >Submit</button>
    </div>
  </form>
)

export default reduxForm({
  form: 'createRental',
  validate,
  fields: ['email', 'password', 'password-confirmation'],
})(CreateRentalForm)
