import React from 'react'
import classnames from 'classnames'

import { Checkbox, FormControl, InputLabel, Select,
  TextField, FormHelperText } from '@material-ui/core/'

export const renderTextField = ({input, symbol, showError, meta: { dirty, touched, error }, ...props}) => (
  <div className={classnames('form-row', {
      'form-control-symbol': symbol,
    })}>
    <input
      className={
        classnames('form-control', {
          error: (showError || touched) && error,
        })
      }
      {...input}
      {...props} />
      {touched &&
      ((error && <span className="danger">{error}</span>))}
    {symbol ? <div className="symbol">{symbol}</div> : null}
  </div>
)

// export const renderTextField = ({
//   label,
//   input,
//   meta: { touched, invalid, error },
//   ...custom
// }) => (
//     <TextField
//       label={label}
//       placeholder={label}
//       error={touched && invalid}
//       helperText={touched && error}
//       {...input}
//       {...custom}
//     />
//   )

export const renderTextareaField = ({input, label, showError, meta: { dirty, touched, error }, ...props}) => (
  <div className={classnames('form-row')}>
    <textarea
      className={
        classnames('form-control', {
          error: (showError || touched) && error
        })
      }
      {...input}
      {...props} />
  </div>
)

export const renderCheckbox = ({ input, label }) => (
  <Checkbox label={label}
    checked={input.value ? true : false}
    onChange={input.onChange} />
)

const renderFromHelper = ({ touched, error }) => {
  if (!(touched && error)) {
    return
  } else {
    return <FormHelperText>{touched && error}</FormHelperText>
  }
}

export const renderSelectField = ({
  input,
  label,
  meta: { touched, error },
  children,
  ...custom
}) => (
    <FormControl error={touched && error}>
      <InputLabel htmlFor="color-native-simple">{label}</InputLabel>
      <Select
        native
        {...input}
        {...custom}
        inputProps={{
          name: input.name,
          id: 'color-native-simple'
        }}
      >
        {children}
      </Select>
      {renderFromHelper({ touched, error })}
    </FormControl>
  )
