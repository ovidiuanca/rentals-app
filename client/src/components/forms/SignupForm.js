import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'

import * as utils from '../../utils'

import {
  renderTextField,
} from './fields'

const validate = (values, props) => {
  const errors = {}

  if (!values.email) {
    errors.email = 'Email is required'
  } else if (values.email && !utils.isEmailValid(values.email)) {
    errors.email = 'Email is not valid'
  }

  if (!values.password) {
    errors.password = 'Password is required'
  } else if (values.password && values.password.length < 4) {
    errors.password = 'Password must be at least 4 characters long'
  }

  if (!values['password-confirmation']) {
    errors['password-confirmation'] = 'Password confirmation is required'
  } else if (values['password-confirmation'] !== values.password) {
    errors['password-confirmation'] = 'Passoword confirmation is wrong'
  }

  return errors
}

class SignupForm extends Component {
  render () {
    const {
      handleSubmit,
      signupFetching,
    } = this.props

    return <div>
      <form className="form-content login-form"
        onSubmit={handleSubmit}
        noValidate>
        <Field name="email" component={renderTextField} placeholder="Email" type="email"/>
        <Field name="password" component={renderTextField} placeholder="Password" type="password"/>
        <Field name="password-confirmation" component={renderTextField} placeholder="Confirm Password" type="password"/>
        <div className="mt-30 right">
          <button
            className="btn w-100"
            disabled={signupFetching}>Sign up</button>
        </div>
      </form>
    </div>
  }
}

export default reduxForm({
  form: 'signup',
  validate,
  fields: ['email', 'password', 'password-confirmation'],
  enableReinitialize: true,
})(SignupForm)
