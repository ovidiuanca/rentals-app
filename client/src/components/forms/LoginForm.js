import React from 'react'
import { Field, reduxForm } from 'redux-form'

import * as utils from '../../utils'

import {
  renderTextField,
} from './fields'

const validate = (values, props) => {
  const errors = {}

  if (!values.email || !utils.isEmailValid(values.email)) {
    errors.email = 'Please input a valid email'
  }

  if (!values.password || values.password.length < 4) {
    errors.password = 'Password must be at least 4 characters long'
  }

  return errors
}

let LoginForm = ({ handleSubmit, loginPending }) => (
  <form onSubmit={handleSubmit}>
    <Field name="email" component={renderTextField} type="text" placeholder="Email" />
    <Field name="password" component={renderTextField} type="password" placeholder="Password" />
    <div className="right mt-30">
      <button
        className="btn w-100"
        type="submit"
        disabled={loginPending}
      >Submit</button>
    </div>
  </form>
)

export default reduxForm({
  form: 'login',
  validate,
  fields: ['email', 'password'],
})(LoginForm)
