import React from 'react'
import { Field, reduxForm } from 'redux-form'

import {
  renderTextField,
  renderTextareaField,
} from './fields'

const validate = (values, props) => {
  const errors = {}

  if (!values.name) {
    errors.description = 'Please input a valid name'
  }

  if (!values.description) {
    errors.description = 'Plase input a valid description'
  }

  if (!values['price-per-month']) {
    errors['price-per-month'] = 'Plase input a valid number for price'
  }

  if (!values['floor-area-size']) {
    errors['floor-area-size'] = 'Plase input a valid number for the floor area size'
  }

  if (!values['number-of-rooms']) {
    errors['number-of-rooms'] = 'Plase input a valid value for the number of rooms'
  }

  if (!values['lat'] || values['lat'] < -90 || values['lat'] > 90) {
    errors['lat'] = 'Plase input a valid value for the lat (-90 <= lat <= 90)'
  }

  if (!values['long'] || values['long'] < -180 || values['long'] > 180) {
    errors['long'] = 'Plase input a valid value for the long (-180 <= long <= 180)'
  }

  return errors
}

let CreateRentalForm = ({ handleSubmit, createPending }) => (
  <form onSubmit={handleSubmit}>
    <label>Name</label>
    <Field name="name" component={renderTextField} type="text" placeholder="Awesome property name" />
    <label>Description</label>
    <Field name="description" component={renderTextareaField} type="textarea" placeholder="Lorem ipsum dolor" />
    <label>Price per month</label>
    <Field name="price-per-month" component={renderTextField} type="number" placeholder="eg: 350 ($)" />
    <label>Floor area size</label>
    <Field name="floor-area-size" component={renderTextField} type="number" placeholder="eg: 1200 (sqm)" />
    <label>Number of rooms</label>
    <Field name="number-of-rooms" component={renderTextField} type="number" placeholder="eg: 3" />
    <label>Latitude</label>
    <Field name="lat" component={renderTextField} type="number" placeholder="eg: 46.75091" />
    <label>Longitude</label>
    <Field name="long" component={renderTextField} type="number" placeholder="eg: 23.553373" />
    <div className="right mt-30">
      <button
        className="btn w-100"
        type="submit"
        disabled={createPending}
      >Submit</button>
    </div>
  </form>
)

export default reduxForm({
  form: 'createRental',
  validate,
  fields: ['name', 'description', 'price-per-month', 'floor-area-size', 'number-of-rooms', 'lat', 'long'],
})(CreateRentalForm)
