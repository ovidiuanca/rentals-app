import RentalService from '../services/RentalService'
import qs from 'qs'

export const getRentals = (search) => {
  return {
    type: 'GET_RENTALS',
    payload: RentalService.getRentals(search)
  }
}

export const resetRentals = () => {
  return {
    type: 'RESET_RENTALS',
  }
}

export const getRental = (rentalId, user) => {
  return {
    type: 'GET_RENTAL',
    payload: RentalService.getRental(rentalId, user)
  }
}

export const createRental = (values, userId) => {
  return {
    type: 'CREATE_RENTAL',
    payload: RentalService.createRental(values, userId)
  }
}

export const updateRental = (rentalId, values, user, realtor) => {
  return {
    type: 'UPDATE_RENTAL',
    payload: RentalService.updateRental(rentalId, values, user),
    meta: { realtor }
  }
}

export const deleteRental = (rentalId) => {
  return {
    type: 'DELETE_RENTAL',
    payload: RentalService.deleteRental(rentalId)
  }
}

export const changeRentalsPage = (currentPage) => {
  return (dispatch) => {
    dispatch({
      type: 'CHANGE_RENTALS_PAGE',
      payload: currentPage
    })
    dispatch({
      type: 'CHANGE_RENTALS_SEARCH',
    })
  }
}

export const changeRentalsFilters = (filters) => {
  return (dispatch) => {
    dispatch({
      type: 'CHANGE_RENTALS_FILTERS',
      payload: filters
    })
    dispatch({
      type: 'CHANGE_RENTALS_SEARCH',
    })
  }
}

export const changeRentalsSearchParams = (search) => {
  let parsedUrl = qs.parse(search.substr(1))

  return {
    type: 'CHANGE_RENTALS_SEARCH_WITH_VALUE',
    search,
    page: parsedUrl.page,
    filter: parsedUrl.filter
  }
}

export const resetShowRental = () => {
  return {
    type: 'RESET_SHOW_RENTAL',
  }
}
