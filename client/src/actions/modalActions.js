export const resetModals = () => {
  return {
    type: 'RESET_MODALS',
  }
}

export const setLoginModalVisibility = (visibility) => {
  return {
    type: 'SET_LOGIN_MODAL_VISIBILITY',
    payload: visibility,
  }
}

export const setSignupModalVisibility = (visibility) => {
  return {
    type: 'SET_SIGNUP_MODAL_VISIBILITY',
    payload: visibility,
  }
}
