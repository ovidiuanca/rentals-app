import { RequestService } from '../store'
import qs from 'qs'

import UserService from '../services/UserService'

export const setInitialUser = () => {
  const user = RequestService.getUser()

  return {
    type: 'SET_USER',
    payload: user,
  }
}

export const login = (data) => {
  return {
    type: 'LOGIN',
    payload: UserService.login(data),
  }
}

export const signUp = (data) => {
  return {
    type: 'SIGN_UP',
    payload: UserService.signUp(data),
  }
}

export const logout = (data) => {
  return {
    type: 'LOGOUT',
  }
}

export const getUsers = (search) => {
  return {
    type: 'GET_USERS',
    payload: UserService.getUsers(search)
  }
}

export const getUser = (userId) => {
  return {
    type: 'GET_USER',
    payload: UserService.getUser(userId)
  }
}

export const createUser = (values) => {
  return {
    type: 'CREATE_USER',
    payload: UserService.signUp(values)
  }
}

export const updateUser = (userId, values) => {
  return {
    type: 'UPDATE_USER',
    payload: UserService.updateUser(userId, values)
  }
}

export const deleteUser = (userId) => {
  return {
    type: 'DELETE_USER',
    payload: UserService.deleteUser(userId)
  }
}

export const resetShowUser = () => {
  return {
    type: 'RESET_SHOW_USER',
  }
}

export const resetUsers = () => {
  return {
    type: 'RESET_USERS',
  }
}

export const changeUsersPage = (currentPage) => {
  return (dispatch) => {
    dispatch({
      type: 'CHANGE_USERS_PAGE',
      payload: currentPage
    })
    dispatch({
      type: 'CHANGE_USERS_SEARCH',
    })
  }
}

export const changeUsersFilters = (filters) => {
  return (dispatch) => {
    dispatch({
      type: 'CHANGE_USERS_FILTERS',
      payload: filters
    })
    dispatch({
      type: 'CHANGE_USERS_SEARCH',
    })
  }
}

export const changeUsersSearchParams = (search) => {
  let parsedUrl = qs.parse(search.substr(1))

  return {
    type: 'CHANGE_USERS_SEARCH_WITH_VALUE',
    search,
    page: parsedUrl.page,
    filter: parsedUrl.filter
  }
}
