import { push, replace } from 'react-router-redux'

export const goToRoute = (route) => {
  return dispatch => {
    dispatch(push(route))
  }
}

export const replaceRoute = (route) => {
  return dispatch => {
    dispatch(replace(route))
  }
}
