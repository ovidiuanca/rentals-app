const defaultState = {
  loginModalVisible: false,
  signupModalVisible: false,
}

export default function reducer (state = defaultState, action) {
  switch (action.type) {
    case 'RESET_MODALS': {
      state = {
        ...defaultState,
      }
      break
    }
    case 'SET_LOGIN_MODAL_VISIBILITY': {
      state = {
        ...state,
        loginModalVisible: action.payload,
      }
      break
    }
    case 'SET_SIGNUP_MODAL_VISIBILITY': {
      state = {
        ...state,
        signupModalVisible: action.payload,
      }
      break
    }
    case 'LOGIN_FULFILLED': {
      state = {
        ...state,
        loginModalVisible: false,
        signupModalVisible: false,
      }
      break
    }
    case 'TRY_LOGIN_FULFILLED':
    case 'SIGN_UP_FULFILLED': {
      state = {
        ...state,
        signupModalVisible: false,
      }
      break
    }
    default: {
      break
    }
  }

  return state
}
