import { combineReducers } from 'redux'
import { loadingBarReducer } from 'react-redux-loading'
import { reducer as formReducer } from 'redux-form'

import user from './userReducer'
import modal from './modalReducer'
import rental from './rentalReducer'

export default combineReducers({
  loadingBar: loadingBarReducer,
  form: formReducer,
  user,
  modal,
  rental,
})
