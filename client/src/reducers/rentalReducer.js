const defaultState = {
  rentals: [],
  rental: {
    attributes: null
  },
  rentalsPending: false,
  rentalPending: false,
  deletePending: false,
  createRentalPending: false,
  totalPages: 0,
  filters: {
    currentPage: 1,
    pageSize: 5,
    maxPrice: null,
    maxRooms: null,
    maxArea: null,
    search: '?page[size]=5&page[number]=1',
  }
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_RENTALS_PENDING': {
      state = {
        ...state,
        rentalsPending: true,
      }

      break
    }
    case 'GET_RENTALS_FULFILLED': {
      const rentals = action.payload && action.payload.data
      const totalPages = action.payload && action.payload.meta && action.payload.meta['page-count']

      state = {
        ...state,
        rentals: rentals,
        rentalsPending: false,
        totalPages,
      }

      break
    }
    case 'GET_RENTALS_REJECTED': {
      state = {
        ...state,
        rentalsPending: false,
      }

      break
    }
    case 'RESET_RENTALS': {
      state = {
        ...state,
        rentals: []
      }

      break
    }
    case 'GET_RENTAL_PENDING': {
      state = {
        ...state,
        rentalPending: true,
      }

      break
    }
    case 'GET_RENTAL_FULFILLED': {
      let rental = action.payload && action.payload.data
      const realtorId = action.payload && action.payload.included && parseInt(action.payload.included[0].id)
      const realtorEmail = action.payload && action.payload.included && action.payload.included[0].attributes.email

      rental['attributes']['realtorId'] = realtorId
      rental['attributes']['realtorEmail'] = realtorEmail

      state = {
        ...state,
        rental: rental,
        rentalPending: false,
      }

      break
    }
    case 'GET_RENTAL_REJECTED': {
      state = {
        ...state,
        rentalPending: false,
      }

      break
    }
    case 'CREATE_RENTAL_PENDING': {
      state = {
        ...state,
        createRentalPending: true,
      }

      break
    }
    case 'CREATE_RENTAL_REJECTED': {
      state = {
        ...state,
        createRentalPending: false,
      }

      break
    }
    case 'CREATE_RENTAL_FULFILLED': {
      state = {
        ...state,
        createRentalPending: false,
      }

      break
    }
    case 'UPDATE_RENTAL_PENDING': {
      state = {
        ...state,
        updatePending: true,
      }

      break
    }
    case 'UPDATE_RENTAL_FULFILLED': {
      let { attributes } = action.payload && action.payload.body &&
        action.payload.body.data

      if (action.meta && action.meta.realtor) {
        attributes['realtorId'] = parseInt(action.meta.realtor.realtorId)
      }

      state = {
        ...state,
        updatePending: false,
        rental: {
          attributes,
        }
      }

      break
    }
    case 'UPDATE_RENTAL_REJECTED': {
      state = {
        ...state,
        updatePending: false,
      }

      break
    }
    case 'DELETE_RENTAL_PENDING': {
      state = {
        ...state,
        deletePending: true,
      }

      break
    }
    case 'DELETE_RENTAL_FULFILLED': {
      const rental = action.payload && action.payload.data

      state = {
        ...state,
        deletePending: false,
      }

      break
    }
    case 'DELETE_RENTAL_REJECTED': {
      state = {
        ...state,
        deletePending: false,
      }

      break
    }
    case 'CHANGE_RENTALS_PAGE': {
      state = {
        ...state,
        filters: {
          ...state.filters,
          currentPage: action.payload
        }
      }

      break
    }
    case 'CHANGE_RENTALS_FILTERS': {
      const { maxPrice, maxRooms, maxArea } = action.payload
      const defaultCurrentPage = defaultState.filters.currentPage

      state = {
        ...state,
        filters: {
          ...state.filters,
          maxPrice,
          maxRooms,
          maxArea,
          currentPage: defaultCurrentPage,
        }
      }

      break
    }
    case 'CHANGE_RENTALS_SEARCH': {
      let search = '?'
      const { currentPage, pageSize, maxPrice, maxRooms, maxArea } = state.filters

      if (currentPage) {
        search += `page[size]=${pageSize}&`
      }
      if (currentPage) {
        search += `page[number]=${currentPage}&`
      }
      if (maxPrice) {
        search += `filter[by_price]=${maxPrice}&`
      }
      if (maxRooms) {
        search += `filter[by_rooms]=${maxRooms}&`
      }
      if (maxArea) {
        search += `filter[by_area]=${maxArea}&`
      }

      search = search.slice(0, -1)

      state = {
        ...state,
        filters: {
          ...state.filters,
          search
        }
      }

      break
    }
    case 'CHANGE_RENTALS_SEARCH_WITH_VALUE': {
      const { page, filter } = action

      let { currentPage, maxPrice, maxArea, maxRooms } = defaultState.filters

      if (page) {
        currentPage = parseInt(page.number)
      }
      if (filter) {
        maxPrice = parseInt(filter.by_price)
        maxArea = parseInt(filter.by_area)
        maxRooms = parseInt(filter.by_rooms)
      }

      state = {
        ...state,
        filters: {
          ...state.filters,
          search: action.search,
          currentPage,
          maxPrice,
          maxArea,
          maxRooms
        }
      }

      break
    }
    case 'RESET_SHOW_RENTAL': {
      state = {
        ...state,
        rental: defaultState.rental
      }

      break
    }
    default: {
      break
    }
  }

  return state
}
