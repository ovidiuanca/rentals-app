import swal from 'sweetalert2/dist/sweetalert2.js'

import { RequestService } from '../store'

const defaultState = {
  user: null,
  users: [],
  showUser: {
    attributes: null
  },
  loginPending: false,
  signupPending: false,
  updatePending: false,
  usersPending: false,
  userPending: false,
  deletePending: false,
  filters: {
    currentPage: 1,
    pageSize: 5,
    search: '?page[size]=5&page[number]=1'
  }
}

export default function reducer (state = defaultState, action) {
  switch (action.type) {
    case 'SET_USER': {
      state = {
        ...state,
        user: action.payload,
      }

      break
    }
    case 'SIGN_UP_PENDING':
    case 'LOGIN_PENDING': {
      state = {
        ...state,
        loginPending: true,
        signupPending: true,
      }

      break
    }
    case 'SIGN_UP_FULFILLED': {
      state = {
        ...state,
        signupPending: false,
        loginPending: false,
      }

      break
    }
    case 'LOGIN_FULFILLED': {
      const user = RequestService.setUser(action.payload)

      state = {
        ...state,
        loginPending: false,
        signupPending: false,
        user: user,
      }

      break
    }
    case 'SIGN_UP_REJECTED':
    case 'LOGIN_REJECTED': {
      state = {
        ...state,
        loginPending: false,
        signupPending: false,
      }

      break
    }
    case 'LOGOUT': {
      RequestService.setUser(null)
      state = {
        ...state,
        user: null,
        userData: null,
      }

      break
    }
    case 'GET_USER_DATA_FULFILLED': {
      state = {
        ...state,
        userData: action.payload,
      }
      break
    }
    case 'CREATE_USER_PENDING': {
      state = {
        ...state,
        createPending: true,
      }
      break
    }
    case 'CREATE_USER_FULFILLED': {
      state = {
        ...state,
        createPending: false,
      }

      break
    }
    case 'CREATE_USER_REJECTED': {
      state = {
        ...state,
        createPending: false,
      }
      break
    }
    case 'UPDATE_USER_PENDING': {
      state = {
        ...state,
        updatePending: true,
      }
      break
    }
    case 'UPDATE_USER_FULFILLED': {
      state = {
        ...state,
        updatePending: false,
        userData: action.payload,
      }

      break
    }
    case 'UPDATE_USER_REJECTED': {
      state = {
        ...state,
        updatePending: false,
      }
      break
    }
    case 'GET_USERS_FULFILLED': {
      const users = action.payload && action.payload.data
      const totalPages = action.payload && action.payload.meta && action.payload.meta['page-count']

      state = {
        ...state,
        usersPending: false,
        users,
        totalPages,
      }
      break
    }
    case 'GET_USERS_REJECTED': {
      state = {
        ...state,
        usersPending: false,
      }
      break
    }
    case 'GET_USERS_PENDING': {
      state = {
        ...state,
        usersPending: true,
      }
      break
    }
    case 'GET_USER_FULFILLED': {
      const showUser = action.payload && action.payload.data

      state = {
        ...state,
        userPending: false,
        showUser,
      }
      break
    }
    case 'GET_USER_REJECTED': {
      state = {
        ...state,
        userPending: false,
      }
      break
    }
    case 'GET_USER_PENDING': {
      state = {
        ...state,
        userPending: true,
      }
      break
    }
    case 'DELETE_USER_FULFILLED': {
      state = {
        ...state,
        deletePending: false,
      }
      break
    }
    case 'DELETE_USER_REJECTED': {
      state = {
        ...state,
        deletePending: false,
      }
      break
    }
    case 'DELETE_USER_PENDING': {
      state = {
        ...state,
        deletePending: true,
      }
      break
    }
    case 'RESET_SHOW_USER': {
      state = {
        ...state,
        showUser: defaultState.showUser
      }
      break
    }
    case 'RESET_USERS': {
      state = {
        ...state,
        users: defaultState.users
      }
      break
    }
    case 'CHANGE_USERS_PAGE': {
      state = {
        ...state,
        filters: {
          ...state.filters,
          currentPage: action.payload
        }
      }

      break
    }
    case 'CHANGE_USERS_FILTERS': {
      const defaultCurrentPage = defaultState.filters.currentPage

      state = {
        ...state,
        filters: {
          ...state.filters,
          currentPage: defaultCurrentPage,
        }
      }

      break
    }
    case 'CHANGE_USERS_SEARCH': {
      let search = '?'
      const { currentPage, pageSize } = state.filters

      if (currentPage) {
        search += `page[size]=${pageSize}&`
      }
      if (currentPage) {
        search += `page[number]=${currentPage}&`
      }

      search = search.slice(0, -1)

      state = {
        ...state,
        filters: {
          ...state.filters,
          search
        }
      }

      break
    }
    case 'CHANGE_USERS_SEARCH_WITH_VALUE': {
      const { page } = action

      let { currentPage } = defaultState.filters

      if (page) {
        currentPage = parseInt(page.number)
      }

      state = {
        ...state,
        filters: {
          ...state.filters,
          search: action.search,
          currentPage,
        }
      }

      break
    }
    default: {
      break
    }
  }

  return state
}
