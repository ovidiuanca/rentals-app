import { buildApi, get, post, destroy, put } from 'redux-bees';
import store from '../store';

const apiEndpoints = {
  login: { method: post, path: '/login' },
  getUsers: { method: get, path: '/users' },
  createUser: { method: post, path: '/users' },
  updateUser: { method: put, path: '/users/:id' },
  deleteUser: { method: destroy, path: '/users/:id' },
  getRentals: { method: get, path: '/rentals' },
  createRental: { method: post, path: '/rentals' },
  updateRental: { method: put, path: '/rentals/:id' },
  deleteRental: { method: destroy, path: '/rentals/:id' },
};

const config = {
  baseUrl: 'http://localhost:3000',
  configureHeaders(headers) {
    const state = store.getState()
    const token = state.user.user && state.user.user.token

    return {
      ...headers,
      'Authorization': `Bearer ${token}`,
    }
  }
};

const api = buildApi(apiEndpoints, config);

export default api
