import { RequestService } from '../store'

import api from './JsonApiService'

class UserService {
  login(data) {
    var outerData = {}
    outerData['data'] = data

    return RequestService.post('/login', null, outerData)
  }

  signUp(data) {
    return api.createUser(
      {
        data: {
          type: 'users',
          attributes: data
        }
      }
    )
  }

  updateUser(id, values) {
    return api.updateUser(
      { id: id },
      {
        data: {
          id: id,
          attributes: values,
          type: 'users',
        }
      }
    )
  }

  getUsers(search) {
    const url = '/users/' + search

    return RequestService.get(url)
  }

  getUser(id) {
    return RequestService.get('/users/' + id)
  }

  deleteUser(id) {
    return RequestService.delete('/users/' + id)
  }
}

export default new UserService()
