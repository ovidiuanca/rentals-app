import axios from 'axios'

class RequestService {
  constructor(store, history) {
    this.store = store

    let apiHost = 'http://localhost:3000'

    this.user = {}
    this.config = {
      apiHost:                    apiHost,
      apiBase:                    '',
      apiVersion:                 '',

      globalConfig: {
        headers: {
          'Content-Type':         'application/vnd.api+json',
          'Accept':               'application/vnd.api+json',
        }
      }
    }
  }

  /*
    Sets and saves the user
    */
  setUser(data) {
    if (data) {
      const content = data.data
      const user = {
        id: content.id,
        email: content.email,
        token: data.jwt,
        role: content.role
      }
      this.user = user
      window.cookies.set('user', user, { path: '/' })

      return user
    } else {
      this.user = null
      window.cookies.remove('user', { path: '/' })
    }
  }

  /*
    Returns user from cookie
    */
  getUser() {
    const user = window.cookies.get('user')
    this.user = user

    return user
  }

  get(url, params, opts) {
    if (params) {
      url = this._addParamsToURL(url, params)
    }

    return this._request(url, 'GET', opts)
  }

  post(url, params, body, opts) {
    if (params) {
      url = this._addParamsToURL(url, params)
    }

    return this._request(url, 'POST', body || {}, opts)
  }

  delete(url, body, opts) {
    return this._request(url, 'DELETE', body, opts)
  }

  put(url, params, body, opts) {
    if (params) {
      url = this._addParamsToURL(url, params)
    }

    return this._request(url, 'PUT', body, opts)
  }

  multipart(url, file) {
    url = this._getApiPath() + url

    const data = new FormData()
    data.append('file', file)

    const options = {
      method: 'POST',
      headers: {
        ...this._composeHeaders(),
        'Content-Type': 'multipart/form-data',
      },
      data: data,
    }

    return new Promise((resolve, reject) => {
      axios(url, options)
      .then((response) => {
        resolve(response.data)
      })
      .catch((err) => {
        reject(err.response)
      })
    })
  }

  _addParamsToURL(url, params) {
    url += '?'
    let first = true

    Object.keys(params).forEach((param) => {
      let value = params[param]
      if (!first) {
        url += '&'
      }
      first = false
      url += param + '=' + value
    })

    return url
  }

  _request(url, method, body, opts) {
    url = this._getApiPath() + url
    opts = opts || {}

    const options = {
      method: method,
      data: JSON.stringify(body),
      ...opts,
      headers: {
        ...this._composeHeaders(),
        ...opts.headers,
      },
    }

    return new Promise((resolve, reject) => {
      axios(url, options)
      .then((response) => {
        resolve(response.data)
      })
      .catch((err) => {
        if (axios.isCancel(err)) {
          reject({
            isCancel: true,
          })
        } else {
          reject(err.response)
        }
      })
    })
  }

  _getApiPath() {
    return this.config.apiHost + this.config.apiBase + this.config.apiVersion
  }

  _composeHeaders() {
    return Object.assign({}, this._getCurrentAuthHeaders(), this.config.globalConfig.headers)
  }

  _getCurrentAuthHeaders() {
    if (this.user && this.user.token) {
      return {
        'Authorization': 'Bearer ' + this.user.token,
      }
    }
  }
}

export default RequestService
