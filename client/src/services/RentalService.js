import { RequestService } from '../store'

import api from './JsonApiService'

class RentalService {
  getRentals(search) {
    const url = '/rentals/' + search

    return RequestService.get(url)
  }

  getRental(id, user) {
    if (user.role === 'admin')
      return RequestService.get(`/rentals/${id}?include=user`)
    else {
      return RequestService.get(`/rentals/${id}`)
    }
  }

  createRental(values, userId) {
    return api.createRental(
      {
        data: {
          type: 'rentals',
          attributes: values,
          relationships: {
            user: {
              data: {
                type: 'users',
                id: userId
              }
            }
          }
        }
      }
    )
  }

  updateRental(id, values, user) {
    const realtorId = parseInt(values['realtorId'])

    delete values['realtorId']
    delete values['realtorEmail']

    if (user.role === 'admin') {
      return api.updateRental(
        { id: id },
        {
          data: {
            id: id,
            attributes: values,
            type: 'rentals',
            relationships: {
              user: {
                data: {
                  type: 'users',
                  id: realtorId
                }
              }
            }
          }
        }
      )
    } else {
      return api.updateRental(
        { id: id },
        {
          data: {
            id: id,
            attributes: values,
            type: 'rentals'
          }
        }
      )
    }
  }

  deleteRental(id) {
    return RequestService.delete('/rentals/' + id)
  }
}

export default new RentalService()
