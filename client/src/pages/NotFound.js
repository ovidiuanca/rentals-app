import React, { Component } from 'react'
import Helmet from 'react-helmet'
import Container from '@material-ui/core/Container'

export default class NotFound extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>
        <Container className="mainContainer">
          <h1 className="mainHeader">404 Not found :(</h1>
        </Container>
      </div>
    )
  }
}
