import React, { Component } from 'react'
import Helmet from 'react-helmet'
import autobind from 'autobind-decorator'
import { connect } from 'react-redux'

import {
  Container, Typography, Grid, Paper, Link, Button
} from '@material-ui/core/';

import CreateUserForm from '../components/forms/CreateUserForm'

import swal from 'sweetalert2/dist/sweetalert2.js'
import * as actions from '../actions'


@connect(state => ({
  createPending: state.user.createPending,
}), actions)
@autobind
export default class CreateUser extends Component {
  constructor(props) {
    super(props)
  }

  handleCreateUser(values) {
    const { history } = this.props

    this.props.createUser(values)
      .then(() => {
        swal({
          title: 'User was created successfully.',
          icon: 'success',
        })
        history.push('/users')
      })
      .catch((err) => {
        if (err && err.body && err.body.errors) {
          swal('ERROR', err.body.errors[0].detail, 'error')
        } else {
          swal('ERROR', 'Something went wrong. Please try again later!', 'error')
        }
      })
  }

  render() {
    const { createPending } = this.props

    return (
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>
        <Container className="mainContainer">
          <h1 className="mainHeader">Create a new user</h1>
          <CreateUserForm
            onSubmit={this.handleCreateUser}
            createPending={createPending}
          />
        </Container>
      </div>
    )
  }
}
