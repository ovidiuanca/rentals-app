import React, { Component } from 'react'
import Helmet from 'react-helmet'
import autobind from 'autobind-decorator'
import { connect } from 'react-redux'

import {
  Container, Typography, Grid, Paper, Link, Button
} from '@material-ui/core/';

import CreateRentalForm from '../components/forms/CreateRentalForm'

import swal from 'sweetalert2/dist/sweetalert2.js'
import * as actions from '../actions'


@connect(state => ({
  createRentalPending: state.rental.createRentalPending,
  currentUser: state.user.user
}), actions)
@autobind
export default class CreateRental extends Component {
  constructor(props) {
    super(props)
  }

  handleCreateRental(values) {
    const { history, currentUser } = this.props
    const userId = currentUser.id

    this.props.createRental(values, userId)
    .then(() =>{
      swal({
        title: 'Rental was created successfully.',
        icon: 'success',
      })
      history.push('/rentals')
    })
    .catch(() => {
      swal({
        title: 'Something went wrong. Please try again.',
        icon: 'error',
      })
    })
  }

  render() {
    const { createRentalPending } = this.props

    return (
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>
        <Container className="mainContainer">
          <h1 className="mainHeader">Create a new rental</h1>
          <CreateRentalForm
            onSubmit={this.handleCreateRental}
            createPending={createRentalPending}
          />
        </Container>
      </div>
    )
  }
}
