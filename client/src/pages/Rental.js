import React, { Component } from 'react'
import Helmet from 'react-helmet'
import autobind from 'autobind-decorator'
import { connect } from 'react-redux'

import * as actions from '../actions'

import EditRentalForm from '../components/forms/EditRentalForm'

import swal from 'sweetalert2/dist/sweetalert2.js'

import pageImage from '../assets/images/rental.jpg'

import Moment from 'react-moment';
import {
  Container, Typography, Grid, Paper, Button
} from '@material-ui/core/';

@connect(state => ({
  user: state.user.user,
  rental: state.rental.rental.attributes,
  updatePending: state.rental.updatePending,
  users: state.user.users,
}), actions)
@autobind
export default class Rental extends Component {
  componentDidMount() {
    const { id } = this.props.match.params
    const { history, user } = this.props

    this.props.getRental(id, user)
      .catch(() => { history.push('/not-found') })

    user.role === 'admin' && this.props.getUsers('')
  }

  componentDidUpdate(prevProps, prevState) {
    const { rental, user } = this.props
    const { id } = this.props.match.params

    if ((prevProps.rental && prevProps.rental.realtorId) !== (rental && rental.realtorId)) {
      this.props.getRental(id, user)
    }
  }

  componentWillUnmount() {
    this.props.resetShowRental()
  }

  handleUpdateRental(values) {
    const { user, history } = this.props
    const { id } = this.props.match.params

    const realtor = {
      realtorId: values['realtorId']
    }

    this.props.updateRental(id, values, user, realtor)
    .then(() =>{
      swal({
        title: 'Rental was updated successfully.',
        icon: 'success',
      })
    })
    .catch(() => {
      swal({
        title: 'Something went wrong. Please try again.',
        icon: 'error',
      })
    })
  }

  handleDeleteRental() {
    const { id } = this.props.match.params

    swal({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
    })
    .then((result) => {
      if (result.value) {
        this.props.deleteRental(id)
        .then(() => {
          swal({
            title: 'Your rental has been deleted!',
            icon: 'success',
          })
          this.props.history.push('/rentals')
        })
        .catch(() => {
          swal({
            title: 'Something went wrong. Please try again.',
            icon: 'error',
          })
        })
      }
    })
  }

  render() {
    const { rental, user, updatePending, users } = this.props

    return (
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>
        <Container className="mainContainer">
          <img className="imgHeader" alt="complex" src={pageImage}/>
          <Paper key={rental && rental.id} className="paper">
            <Grid container spacing={2}>
              <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={2}>
                  <Grid item xs>
                    <Typography gutterBottom variant="subtitle1">
                      {rental && rental.name}
                    </Typography>
                    <Typography variant="body2" gutterBottom>
                      {rental && rental.description}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      • Added: <Moment fromNow>{rental && rental['created-at']}</Moment> •
                      Rooms: {rental && rental['number-of-rooms']} •
                      Floor area: {rental && rental['floor-area-size']} sqm •
                    </Typography>
                    { user.role === 'admin' &&
                      <Typography variant="body2" gutterBottom>
                        Added by: {rental && rental.realtorEmail}
                      </Typography>
                    }
                  </Grid>
                  <Grid item>
                    {rental && rental['is-available']
                      ?
                      <Typography variant="body2" className="success">
                        Available
                      </Typography>
                      :
                      <Typography variant="body2" className="danger">
                        Not available
                      </Typography>
                    }
                  </Grid>
                </Grid>
                <Grid item>
                  <Typography className="rentalPrice" variant="subtitle1">
                    ${rental && rental['price-per-month']}
                    <Typography color="textSecondary" variant="body2">per month</Typography>
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
          { user.role !== 'client' &&
            <div>
              <EditRentalForm
                onSubmit={this.handleUpdateRental}
                updatePending={updatePending}
                initialValues={rental}
                enableReinitialize={true}
                users={users}
                user = {user}
              />
              <Button className="deleteButton" variant="contained" color="secondary" onClick={this.handleDeleteRental}>
                Delete rental
              </Button>
            </div>
          }
        </Container>
      </div>
    )
  }
}
