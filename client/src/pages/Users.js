import React, { Component } from 'react'
import Helmet from 'react-helmet'
import autobind from 'autobind-decorator'
import { connect } from 'react-redux'
import {
  Table, TableBody, TableCell, Paper, Container,
  TableContainer, TableHead, TableRow, Button, Grid
} from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';

import { Link } from 'react-router-dom'

import * as actions from '../actions'

@connect(state => ({
  users: state.user.users,
  currentPage: state.user.filters.currentPage,
  totalPages: state.user.totalPages,
  search: state.user.filters.search,
}), actions)
@autobind
export default class Users extends Component {
  componentDidMount() {
    const { search } = this.props

    this.props.getUsers(search)
  }

  componentWillMount() {
    this.updateSearchFromMeta()
  }

  componentDidUpdate(prevProps, prevState) {
    const { search, history } = this.props

    if (prevProps.search !== search) {
      this.props.getUsers(search)

      history.push(search)
    }
  }

  componentWillUnmount() {
    this.props.resetUsers()
  }

  updateSearchFromMeta() {
    const queryParams = this.props.location && this.props.location.search

    if (queryParams) {
      this.props.changeUsersSearchParams(queryParams)
    }
  }

  handlePageChange(_, value) {
    this.props.changeUsersPage(value)
  }

  render() {
    const { users, currentPage, totalPages } = this.props

    const userRows = users.map((user) => ({ id: user.id, email: user.attributes.email, role: user.attributes.role }))

    return (
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>

        <Container className="mainContainer">
          <h1 className="mainHeader">Users</h1>

          <Link to='/create-user'>
            <Button variant="contained" className="createUserButton">
              Create a new user
            </Button>
          </Link>

          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>Role</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {userRows.map((row) => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {row.id}
                    </TableCell>
                    <TableCell>
                      <Link color="textPrimary" to={`/user/${row.id}`}>
                        {row.email}
                      </Link>
                    </TableCell>
                    <TableCell>{row.role}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className="paginationItem"
        >
          { users.length ?
            <Pagination page={currentPage} count={totalPages} onChange={this.handlePageChange} size="large" />
          :
            <p>No users were found</p>
          }
        </Grid>
      </div>
    )
  }
}
