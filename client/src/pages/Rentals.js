import React, { Component } from 'react'
import Helmet from 'react-helmet'
import autobind from 'autobind-decorator'
import { connect } from 'react-redux'
import Moment from 'react-moment';
import {
  Container, Typography, Grid, Paper, Button
} from '@material-ui/core/';
import Pagination from '@material-ui/lab/Pagination';
import { Link } from 'react-router-dom'

import * as actions from '../actions'
import RentalFilter from '../components/RentalFilter'

import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = 'pk.eyJ1Ijoib3ZpZGl1YW5jYSIsImEiOiJjazh1aTBobmwwN3U1M25rMjlsZjVkYWdjIn0.LNBFXwG2z61KfR5ikvzLFg';

@connect(state => ({
  user: state.user.user,
  rentals: state.rental.rentals,
  currentPage: state.rental.filters.currentPage,
  totalPages: state.rental.totalPages,
  search: state.rental.filters.search
}), actions)
@autobind
export default class Rentals extends Component {
  constructor(props) {
    super(props)

    this.state = {
      markers: []
    };
  }

  componentWillMount() {
    this.updateSearchFromMeta()
  }

  componentDidMount() {
    this.initializeMap()
    this.getRentals()
  }

  componentDidUpdate(prevProps, prevState) {
    const { search, rentals, history } = this.props

    if (prevProps.search !== search) {
      this.getRentals()

      history.push(search)
    }
    if (prevProps.rentals !== rentals) {
      this.updateMarkers()
    }
  }

  componentWillUnmount() {
    this.props.resetRentals()
  }

  updateSearchFromMeta() {
    const queryParams = this.props.location && this.props.location.search

    if (queryParams) {
      this.props.changeRentalsSearchParams(queryParams)
    }
  }

  updateMarkers() {
    const { rentals } = this.props

    this.state.markers.forEach(marker => {
      marker.remove()
    })

    const markers = rentals.map((rental) => {
      var el = document.createElement('div');
      el.className = 'marker';

      const popup = new mapboxgl.Popup({ offset: 25 })
        .setHTML(
          '<a href="/rental/' + rental.id + '"><h3>' + rental.attributes.name + '</h3><p>$'
          + rental.attributes['price-per-month'] + '</p></a>'
          );

      const marker = new mapboxgl.Marker(el)
        .setLngLat([rental.attributes.long, rental.attributes.lat])
        .setPopup(popup)
        .addTo(this.map);
      return marker
    });

    this.setState({
      markers: [...markers],
    })
  }

  initializeMap() {
    this.map = new mapboxgl.Map({
      container: this.mapContainer,
      style: 'mapbox://styles/mapbox/light-v10',
      center: [23.596523, 46.768756],
      zoom: 11
    });
  }

  handlePageChange(_, value) {
    this.props.changeRentalsPage(value)
  }

  getRentals() {
    const { search } = this.props

    this.props.getRentals(search)
  }

  render() {
    const { currentPage, totalPages, rentals, user } = this.props

    return (
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>
        <div ref={el => this.mapContainer = el} className="mapContainer" />
        { rentals.length ?
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className="paginationItem"
          >
            <Pagination page={currentPage} count={totalPages} onChange={this.handlePageChange} size="large" />
        </Grid>
        :
          null
        }
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className="paginationItem"
        >
          <RentalFilter />
        </Grid>
        <Container className="mainContainer">
          { user.role !== 'client' &&
            <Link to='/create-rental'>
              <Button variant="contained" className="rentalButton">
                Create a new rental
              </Button>
            </Link>
          }

          <div>
            {rentals.map((rental) =>
              <Paper key={rental.id} className="paper">
                <Grid container spacing={2}>
                  <Grid item>
                    <Link to={`/rental/${rental.id}`}>
                      <img alt="complex" src="https://via.placeholder.com/128" />
                    </Link>
                  </Grid>
                  <Grid item xs={12} sm container>
                    <Grid item xs container direction="column" spacing={2}>
                      <Grid item xs>
                        <Typography gutterBottom variant="subtitle1">
                          <Link color="textPrimary" to={`/rental/${rental.id}`}>
                            {rental.attributes.name}
                          </Link>
                        </Typography>
                        <Typography variant="body2" gutterBottom>
                          {rental.attributes.description}
                        </Typography>
                        <Typography variant="body2" color="textSecondary">
                          • Added: <Moment fromNow>{rental.attributes['created-at']}</Moment> •
                          Rooms: {rental.attributes['number-of-rooms']} •
                          Floor area: {rental.attributes['floor-area-size']} sqm •
                        </Typography>
                      </Grid>
                      <Grid item>
                        {rental.attributes['is-available']
                          ?
                          <Typography variant="body2" className="success">
                            Available
                          </Typography>
                          :
                          <Typography variant="body2" className="danger">
                            Not available
                          </Typography>
                        }
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Typography className="rentalPrice" variant="subtitle1">
                        ${rental.attributes['price-per-month']}
                        <Typography color="textSecondary" variant="body2">per month</Typography>
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Paper>
            )}
          </div>
        </Container>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className="paginationItem"
        >
          { rentals.length ?
            <Pagination page={currentPage} count={totalPages} onChange={this.handlePageChange} size="large" />
          :
            <p>No rentals were found</p>
          }
        </Grid>
      </div>
    )
  }
}
