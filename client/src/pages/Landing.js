import React, { Component } from 'react'
import Helmet from 'react-helmet'

import Container from '@material-ui/core/Container'

export default class Landing extends Component {
  render() {
    return (
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>
        <Container>
          <h1 className="mainHeader">Rentals app</h1>
        </Container>
      </div>
    )
  }
}
