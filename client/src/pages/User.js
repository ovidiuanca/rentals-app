import React, { Component } from 'react'
import Helmet from 'react-helmet'
import autobind from 'autobind-decorator'
import swal from 'sweetalert2/dist/sweetalert2.js'
import { connect } from 'react-redux'
import {
  Container, Typography, Grid, Paper, Button
} from '@material-ui/core/';

import * as actions from '../actions'

import EditUserForm from '../components/forms/EditUserForm'

@connect(state => ({
  user: state.user.showUser.attributes,
  updatePending: state.user.updatePending,
}), actions)
@autobind
export default class User extends Component {
  componentDidMount() {
    const { id } = this.props.match.params
    const { history } = this.props

    this.props.getUser(id)
      .catch(() => { history.push('/not-found') })
  }

  componentWillUnmount() {
    this.props.resetShowUser()
  }

  handleUpdateUser(values) {
    const { id } = this.props.match.params

    this.props.updateUser(id, values)
    .then(() =>{
      swal({
        title: 'User was updated successfully.',
        icon: 'success',
      })
    })
    .catch(() => {
      swal({
        title: 'Something went wrong. Please try again.',
        icon: 'error',
      })
    })
  }

  handleDeleteUser() {
    const { id } = this.props.match.params

    swal({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
    })
    .then((result) => {
      if (result.value) {
        this.props.deleteUser(id)
        .then(() => {
          swal({
            title: 'User has been deleted!',
            icon: 'success',
          })
          this.props.history.push('/users')
        })
        .catch(() => {
          swal({
            title: 'Something went wrong. Please try again.',
            icon: 'error',
          })
        })
      }
    })
  }

  render() {
    const {
      user,
      updatePending,
    } = this.props

    return (
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>
        <Container className="mainContainer">
          <h1 className="mainHeader">Edit user</h1>
          <EditUserForm
            onSubmit={this.handleUpdateUser}
            updatePending={updatePending}
            initialValues={user}
          />
          <Button className="deleteButton" variant="contained" color="secondary" onClick={this.handleDeleteUser}>
            Delete user
          </Button>
        </Container>
      </div>
    )
  }
}
