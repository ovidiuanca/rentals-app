import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import autobind from 'autobind-decorator'
import Helmet from 'react-helmet'

import Modals from './components/modals'
import Header from './components/Header'
import Landing from './pages/Landing'
import Rentals from './pages/Rentals'
import Rental from './pages/Rental'
import CreateRental from './pages/CreateRental'
import Users from './pages/Users'
import User from './pages/User'
import CreateUser from './pages/CreateUser'
import NotFound from './pages/NotFound'
import './config/initializers'
import * as actions from './actions'

const IsAuthenticated = ({ user }) => {
  if (user) {
    if (user.role === 'admin') {
      return(<IsAdmin />)
    } else if (user.role === 'realtor') {
      return(<IsRealtor />)
    } else if (user.role === 'client') {
      return(<IsClient />)
    }
  } else {
    return (<NotFound />)
  }
}

const IsClient = () => {
  return(
    <div>
      <Switch>
        <Route exact path="/rentals" component={Rentals} />
        <Route exact path="/rental/:id" component={Rental} />
        <Route component={NotFound} />
      </Switch>
    </div>
  )
}

const IsRealtor = () => {
  return(
    <div>
      <Switch>
        <Route exact path="/rentals" component={Rentals} />
        <Route exact path="/rental/:id" component={Rental} />
        <Route exact path="/create-rental" component={CreateRental} />
        <Route component={NotFound} />
      </Switch>
    </div>
  )
}

const IsAdmin = () => {
  return(
    <div>
      <Switch>
        <Route exact path="/rentals" component={Rentals} />
        <Route exact path="/rental/:id" component={Rental} />
        <Route exact path="/create-rental" component={CreateRental} />
        <Route exact path="/users" component={Users} />
        <Route exact path="/create-user" component={CreateUser} />
        <Route exact path="/user/:id" component={User} />
        <Route component={NotFound} />
      </Switch>
    </div>
  )
}

@withRouter
@connect(state => ({
  user: state.user.user,
}), actions)
@autobind
export default class Routes extends Component {
  componentWillMount() {
    const {
      setInitialUser,
    } = this.props

    setInitialUser()
  }

  componentDidUpdate(prevProps) {
    const {
      resetModals,
      location,
    } = this.props

    if (prevProps.location.pathname !== location.pathname) {
      resetModals()
      setTimeout(() => {
        window.scrollTo(0, 0)
      })
    }
  }

  render() {
    const {
      user,
      setLoginModalVisibility,
      setSignupModalVisibility,
      logout,
    } = this.props

    return(
      <div>
        <Helmet>
          <title>Rentals app</title>
          <meta name="description" content="Rentals app" />
        </Helmet>
        <Modals />
        <Header
          user={user}
          setLoginModalVisibility={setLoginModalVisibility}
          setSignupModalVisibility={setSignupModalVisibility}
          logout={logout}
        />
        <Switch>
          <Route exact path="/" component={Landing} />
          <IsAuthenticated user={ user } />
        </Switch>
      </div>
    )
  }
}
