import { applyMiddleware, createStore, compose } from 'redux'
import promise from 'redux-promise-middleware'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'

import reducer from './reducers'
import RS from './services/RequestService'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const history = createHistory()
const historyMiddleware = routerMiddleware(history)
const middleware = applyMiddleware(
  historyMiddleware,
  thunk,
  promise(),
)

export const RequestService = new RS()

export default createStore(reducer, composeEnhancers(middleware))
