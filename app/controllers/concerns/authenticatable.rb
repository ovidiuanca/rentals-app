# frozen_string_literal: true

module Authenticatable
  extend ActiveSupport::Concern

  class UnauthenticatedError < StandardError
    def message
      'Access denied. You are not authorized to access the requested page.'
    end
  end

  included do
    helper_method :current_user

    rescue_from UnauthenticatedError do |exception|
      @current_user = nil

      render json: { error: exception.message }, status: :unauthorized
    end
  end

  def authenticate_user!
    raise UnauthenticatedError unless current_user

    true
  end

  def current_user
    @current_user ||= begin
      return if jwt_user_email.blank?

      User.find_by_email(jwt_user_email)
    end
  end

  private

  def jwt_user_email
    jwt_data&.dig('email')
  end

  def jwt_data
    return if header_token.blank?

    @jwt_data ||= JsonWebToken.decode(header_token)
  rescue JWT::DecodeError, JWT::VerificationError
    nil
  end

  def header_token
    header = request.headers['Authorization']
    return if header.blank?

    _type, token = header.split(' ', 2)
    token
  end
end
