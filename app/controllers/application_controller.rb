# frozen_string_literal: true

class ApplicationController < ActionController::API
  include JSONAPI::ActsAsResourceController
  include Pundit
  include ActionController::Helpers
  include Authenticatable

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :authenticate_user!

  private

  def context
    { current_user: current_user }
  end

  def user_not_authorized
    head :forbidden
  end
end
