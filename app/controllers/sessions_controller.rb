# frozen_string_literal: true

class SessionsController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[create]

  def create
    user = User.find_by(email: permitted_params[:email])

    if user&.authenticate(permitted_params[:password])
      payload = { user_id: user.id, email: user.email, role: user.role }
      token = JsonWebToken.encode(payload)

      render json: { data: user, jwt: token }, status: 201
    else
      render json: { error: 'Wrong email or password' }, status: 401
    end
  end

  private

  def permitted_params
    params.require(:data).permit(:email, :password)
  end
end
