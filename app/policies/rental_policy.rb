# frozen_string_literal: true

class RentalPolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    user.admin? || user.realtor?
  end

  def update?
    user.admin? || user.realtor?
  end

  def show?
    true
  end

  def destroy?
    user.admin? || user.realtor?
  end

  class Scope < Scope
    def resolve
      if user.client?
        scope.available
      else
        scope
      end
    end
  end
end
