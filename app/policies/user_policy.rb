# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def index?
    user.admin?
  end

  def create?
    true
  end

  def update?
    user.admin? || user.realtor?
  end

  def show?
    user.admin?
  end

  def destroy?
    user.admin?
  end
end
