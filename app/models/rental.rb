# frozen_string_literal: true

class Rental < ApplicationRecord
  validates :name, :description, :floor_area_size, :number_of_rooms,
            :price_per_month, :lat, :long,
            presence: true

  validates_inclusion_of :lat, in: -90..90
  validates_inclusion_of :long, in: -180..180

  belongs_to :user

  scope :available, -> { where(is_available: true) }

  def available?
    is_available
  end
end
