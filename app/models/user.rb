# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  enum role: %i[client realtor admin]

  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }

  has_many :rentals
end
