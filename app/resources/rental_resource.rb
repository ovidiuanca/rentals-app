# frozen_string_literal: true

class RentalResource < JSONAPI::Resource
  include ::JSONAPI::Authorization::PunditScopedResource

  attribute :name
  attribute :description
  attribute :price_per_month
  attribute :floor_area_size
  attribute :number_of_rooms
  attribute :lat
  attribute :long
  attribute :created_at
  attribute :is_available

  has_one :user

  paginator :paged

  def self.default_sort
    [{ field: 'created_at', direction: :desc }]
  end

  filter :by_price, apply: ->(records, value, _options) {
    records.where('price_per_month <= ?', value[0])
  }

  filter :by_rooms, apply: ->(records, value, _options) {
    records.where('number_of_rooms <= ?', value[0])
  }

  filter :by_area, apply: ->(records, value, _options) {
    records.where('floor_area_size <= ?', value[0])
  }
end
