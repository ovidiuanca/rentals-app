# frozen_string_literal: true

class UserResource < JSONAPI::Resource
  include ::JSONAPI::Authorization::PunditScopedResource

  attribute :email
  attribute :password
  attribute :password_confirmation
  attribute :role

  def fetchable_fields
    super - %i[password password_confirmation]
  end

  def self.creatable_fields(context = nil)
    user = context[:current_user]

    fields = super
    fields -= %i[role] unless user&.admin?

    fields
  end

  def self.updatable_fields(context = nil)
    user = context[:current_user]

    fields = super
    fields -= %i[email password role] unless user&.admin?

    fields
  end

  paginator :paged
end
