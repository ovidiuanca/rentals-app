# # frozen_string_literal: true

admin = User.find_by(email: 'admin@example.com')
admin ||= User.create!(
  email: 'admin@example.com',
  password: '12345678',
  password_confirmation: '12345678',
  role: 'admin'
)

client = User.find_by(email: 'client@example.com')
client ||= User.create!(
  email: 'client@example.com',
  password: '12345678',
  password_confirmation: '12345678',
  role: 'client'
)

realtor = User.find_by(email: 'realtor@example.com')
realtor ||= User.create!(
  email: 'realtor@example.com',
  password: '12345678',
  password_confirmation: '12345678',
  role: 'realtor'
)

30.times do
  Rental.create!(
    name: Faker::Address.full_address,
    description: Faker::Lorem.paragraph,
    floor_area_size: Faker::Number.between(from: 300, to: 5000),
    number_of_rooms: Faker::Number.between(from: 1, to: 5),
    price_per_month: Faker::Number.between(from: 300, to: 1500),
    lat: rand(46.740331...46.797004),
    long: rand(23.538398...23.630813),
    user: [admin, realtor].sample,
    is_available: Faker::Boolean.boolean
  )
end
