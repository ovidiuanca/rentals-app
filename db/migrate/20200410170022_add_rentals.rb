class AddRentals < ActiveRecord::Migration[6.0]
  def change
    create_table :rentals do |t|
      t.string  :name
      t.text    :description
      t.integer :floor_area_size
      t.integer :price_per_month
      t.integer :number_of_rooms
      t.float   :lat
      t.float   :long
      t.integer :user_id

      t.index %i[lat long]

      t.timestamps
    end
  end
end
