# frozen_string_literal: true

require 'rails_helper'

describe Rental, type: :model do
  it { should validate_presence_of :name }
  it { should validate_presence_of :description }
  it { should validate_presence_of :price_per_month }
  it { should validate_presence_of :floor_area_size }
  it { should validate_presence_of :number_of_rooms }
  it { should validate_presence_of :lat }
  it { should validate_presence_of :long }
  it { should belong_to :user }

  context 'when lat is invalid' do
    let(:rental) { build(:rental) }

    before { rental.lat = 95 }

    it 'should be invalid' do
      expect(rental).not_to be_valid
    end
  end

  context 'when long is invalid' do
    let(:rental) { build(:rental) }

    before { rental.long = -195 }

    it 'should be invalid' do
      expect(rental).not_to be_valid
    end
  end

  context 'when lat is valid' do
    let(:rental) { build(:rental) }

    before {  }

    it 'should be valid' do
      rental.lat = -85
      expect(rental).to be_valid

      rental.lat = -90
      expect(rental).to be_valid

      rental.lat = 90
      expect(rental).to be_valid
    end
  end

  context 'when long is valid' do
    let(:rental) { build(:rental) }

    it 'should be valid' do
      rental.long = 175
      expect(rental).to be_valid

      rental.long = -180
      expect(rental).to be_valid

      rental.long = 180
      expect(rental).to be_valid
    end
  end
end
