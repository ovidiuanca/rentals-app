# frozen_string_literal: true

require 'rails_helper'

describe User, type: :model do
  it { should validate_presence_of :email }
  it { should validate_uniqueness_of(:email).case_insensitive }
  it { should validate_presence_of :password }

  it 'is invalid with invalid email' do
    user = User.new(
      email: 'invalid email',
      password: '123456',
      password_confirmation: '123456'
    )

    expect(user).to be_invalid
  end
end
