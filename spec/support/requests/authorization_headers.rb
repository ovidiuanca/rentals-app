# frozen_string_literal: true

module AuthorizationHeaders
  ACCEPT_HEADER_VALUE = 'application/vnd.api+json'

  def authorization_headers(user = nil)
    headers = {
      'Content-Type' => ACCEPT_HEADER_VALUE
    }

    if user
      payload = {
        user_id: user.id,
        email: user.email
      }

      token = JsonWebToken.encode(payload)

      headers['Authorization'] = "Bearer #{token}"
    end

    headers
  end
end
