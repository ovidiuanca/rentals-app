# frozen_string_literal: true

require 'rails_helper'

describe 'DELETE /rentals/:id', type: :request do
  let(:endpoint) { "/rentals/#{rental.id}" }

  context 'unauthenticated' do
    let(:current_user) { nil }
    let!(:rental) { create(:rental) }
    let(:expected_status) { 401 }

    it 'returns expected status' do
      delete endpoint, headers: authorization_headers(current_user)
      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    subject { JSON.parse(response.body)['data'] }

    context 'when admin' do
      let!(:current_user) { create(:user, :admin) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 204 }

      it 'returns expected status' do
        delete endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end

      it 'deletes rental' do
        expect do
          delete endpoint, headers: authorization_headers(current_user)
        end.to change { Rental.count }.from(1).to(0)
      end
    end

    context 'when realtor' do
      let!(:current_user) { create(:user, :realtor) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 204 }

      it 'returns expected status' do
        delete endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end

      it 'deletes rental' do
        expect do
          delete endpoint, headers: authorization_headers(current_user)
        end.to change { Rental.count }.from(1).to(0)
      end
    end

    context 'when client' do
      let!(:current_user) { create(:user, :client) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 403 }

      it 'returns expected status' do
        delete endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end
    end
  end
end
