# frozen_string_literal: true

require 'rails_helper'

describe 'POST /rentals', type: :request do
  let(:endpoint) { '/rentals' }
  let!(:user) { create(:user) }
  let(:example_rental) { build(:rental) }
  let(:body) do
    {
      data: {
        type: 'rentals',
        attributes: {
          name: example_rental.name,
          description: example_rental.description,
          'floor-area-size': example_rental.floor_area_size,
          'number-of-rooms': example_rental.number_of_rooms,
          'price-per-month': example_rental.price_per_month,
          lat: example_rental.lat,
          long: example_rental.long
        },
        relationships: {
          user: { data: { type: 'users', id: user.id } }
        }
      }
    }
  end

  context 'unauthenticated' do
    let(:current_user) { nil }
    let(:expected_status) { 401 }

    it 'returns expected status' do
      post endpoint, headers: authorization_headers(current_user), params: body.to_json

      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    subject { JSON.parse(response.body)['data'] }

    context 'when admin' do
      let(:current_user) { create(:user, :admin) }
      let(:expected_status) { 201 }

      it 'returns expected status' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end

      it 'creates proper rental' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(Rental.last.name).to eq(example_rental.name)
        expect(Rental.last.user).to eq(user)
      end
    end

    context 'when realtor' do
      let(:current_user) { create(:user, :realtor) }
      let(:expected_status) { 201 }

      it 'returns expected status' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end

      it 'creates proper rental' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(Rental.last.name).to eq(example_rental.name)
        expect(Rental.last.user).to eq(user)
      end
    end

    context 'when client' do
      let(:current_user) { create(:user, :client) }
      let(:expected_status) { 403 }

      it 'returns expected status' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end
    end
  end
end
