# frozen_string_literal: true

require 'rails_helper'

describe 'PUT /rentals/:id', type: :request do
  let(:endpoint) { "/rentals/#{rental.id}" }
  let(:new_rental) { build(:rental) }
  let(:new_user) { create(:user) }

  let(:body) do
    {
      data: {
        id: rental.id,
        type: 'rentals',
        attributes: {
          name: 'updated name',
          description: 'updated_description',
          'floor-area-size': 1024,
          'number-of-rooms': 4,
          'price-per-month': 250,
          lat: new_rental.lat,
          long: new_rental.long
        },
        relationships: {
          user: { data: { type: 'users', id: new_user.id } }
        }
      }
    }
  end

  context 'unauthenticated' do
    let(:current_user) { nil }
    let!(:rental) { create(:rental) }
    let(:expected_status) { 401 }

    it 'returns expected status' do
      put endpoint, headers: authorization_headers(current_user), params: body.to_json
      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    context 'when admin' do
      let!(:current_user) { create(:user, :admin) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 200 }

      it 'returns expected status' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json
        expect(response.status).to eq(expected_status)
      end

      it 'updates proper rental' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json
        expect(rental.reload.name).to eq('updated name')
      end
    end

    context 'when realtor' do
      let!(:current_user) { create(:user, :realtor) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 200 }

      it 'returns expected status' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json
        expect(response.status).to eq(expected_status)
      end

      it 'updates proper rental' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json
        expect(rental.reload.name).to eq('updated name')
      end
    end

    context 'when client' do
      let!(:current_user) { create(:user, :client) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 403 }

      it 'returns expected status' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json
        expect(response.status).to eq(expected_status)
      end
    end
  end
end
