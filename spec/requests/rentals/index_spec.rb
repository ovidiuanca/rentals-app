# frozen_string_literal: true

require 'rails_helper'

describe 'GET /rentals', type: :request do
  let(:endpoint) { '/rentals' }

  context 'unauthenticated' do
    let(:current_user) { nil }
    let(:data) { create_list(:rental, 3) }
    let(:expected_status) { 401 }

    before { data }

    it 'returns expected status' do
      get endpoint, headers: authorization_headers(current_user)
      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    subject { JSON.parse(response.body)['data'] }

    context 'when admin' do
      let(:current_user) { create(:user, :admin) }
      let(:rentals) { create_list(:rental, 3) }
      let(:not_available_rentals) { 3.times { create(:rental, :not_available) } }
      let(:expected_status) { 200 }
      let(:expected_count) { 6 }

      before do
        rentals
        not_available_rentals
      end

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end

      it 'returns expected number of records' do
        get endpoint, headers: authorization_headers(current_user)
        expect(subject.size).to eq(expected_count)
      end
    end

    context 'when realtor' do
      let(:current_user) { create(:user, :realtor) }
      let(:rentals) { create_list(:rental, 3) }
      let(:not_available_rentals) { 3.times { create(:rental, :not_available) } }
      let(:expected_status) { 200 }
      let(:expected_count) { 6 }

      before do
        rentals
        not_available_rentals
      end

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end

      it 'returns expected number of records' do
        get endpoint, headers: authorization_headers(current_user)
        expect(subject.size).to eq(expected_count)
      end
    end

    context 'when client' do
      let(:current_user) { create(:user, :client) }
      let(:rentals) { create_list(:rental, 3) }
      let(:not_available_rentals) { 3.times { create(:rental, :not_available) } }
      let(:expected_status) { 200 }
      let(:expected_count) { 3 }

      before do
        rentals
        not_available_rentals
      end

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end

      it 'returns expected number of records' do
        get endpoint, headers: authorization_headers(current_user)
        expect(subject.size).to eq(expected_count)
      end
    end
  end
end
