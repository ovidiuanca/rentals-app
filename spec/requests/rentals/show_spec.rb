# frozen_string_literal: true

require 'rails_helper'

describe 'GET /rentals/:id', type: :request do
  let(:endpoint) { "/rentals/#{rental.id}" }

  context 'unauthenticated' do
    let(:current_user) { nil }
    let(:rental) { build(:rental) }
    let(:expected_status) { 401 }

    it 'returns expected status' do
      get endpoint, headers: authorization_headers(current_user)
      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    subject { JSON.parse(response.body)['data'] }

    context 'when admin' do
      let!(:current_user) { create(:user, :admin) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 200 }

      it 'returns expected status when rental is availalbe' do
        get endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end

      it 'returns expected rental' do
        get endpoint, headers: authorization_headers(current_user)
        expect(subject['id']).to eq(rental.id.to_s)
      end

      it 'returns expected status when rental is not availalbe' do
        rental.update(is_available: false)

        get endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end
    end

    context 'when realtor' do
      let!(:current_user) { create(:user, :realtor) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 200 }

      it 'returns expected status when rental is availalbe' do
        get endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end

      it 'returns expected rental' do
        get endpoint, headers: authorization_headers(current_user)
        expect(subject['id']).to eq(rental.id.to_s)
      end

      it 'returns expected status when rental is not availalbe' do
        rental.update(is_available: false)

        get endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end
    end

    context 'when client' do
      let!(:current_user) { create(:user, :client) }
      let!(:rental) { create(:rental) }
      let(:expected_status) { 200 }

      it 'returns expected status when rental is availalbe' do
        get endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end

      it 'returns expected rental' do
        get endpoint, headers: authorization_headers(current_user)
        expect(subject['id']).to eq(rental.id.to_s)
      end

      it 'returns expected status when rental is not availalbe' do
        rental.update(is_available: false)

        get endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(404)
      end
    end
  end
end
