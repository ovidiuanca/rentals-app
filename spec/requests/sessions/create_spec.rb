# frozen_string_literal: true

require 'rails_helper'

describe 'POST /login', type: :request do
  let(:endpoint) { '/login' }

  subject { JSON.parse(response.body) }

  context 'when user is correct' do
    let!(:user) { create(:user, email: 'test@example.com') }
    let(:current_user) { nil }
    let(:body) do
      {
        data: {
          email: 'test@example.com',
          password: '12345678'
        }
      }
    end
    let(:expected_status) { 201 }

    it 'returns expected status' do
      post endpoint, headers: authorization_headers(nil), params: body.to_json
      expect(response.status).to eq(expected_status)
    end

    it 'returns JWT' do
      post endpoint, headers: authorization_headers(nil), params: body.to_json
      expect(subject['jwt']).not_to be_empty
    end

    it 'returns JWT' do
      post endpoint, headers: authorization_headers(nil), params: body.to_json
      expect(subject['data']['email']).to eq(user.email)
    end
  end

  context 'when user is incorrect' do
    let!(:user) { create(:user, email: 'test@example.com') }
    let(:current_user) { nil }
    let(:body) do
      {
        data: {
          email: 'incorrect@example.com',
          password: 'incorrect password'
        }
      }
    end
    let(:expected_status) { 401 }

    it 'returns expected status' do
      post endpoint, headers: authorization_headers(nil), params: body.to_json
      expect(response.status).to eq(expected_status)
    end

    it 'returns proper error' do
      post endpoint, headers: authorization_headers(nil), params: body.to_json
      expect(subject['error']).to eq('Wrong email or password')
    end
  end
end
