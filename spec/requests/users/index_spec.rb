# frozen_string_literal: true

require 'rails_helper'

describe 'GET /users', type: :request do
  let(:endpoint) { '/users' }

  context 'unauthenticated' do
    let(:current_user) { nil }
    let(:data) { create_list(:user, 2) }
    let(:expected_status) { 401 }

    before { data }

    it 'returns expected status' do
      get endpoint, headers: authorization_headers(current_user)
      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    subject { JSON.parse(response.body)['data'] }

    context 'when admin' do
      let(:current_user) { create(:user, :admin) }
      let(:data) { create_list(:user, 5) }
      let(:expected_status) { 200 }
      let(:expected_count) { 6 }

      before { data }

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end

      it 'returns expected number of records' do
        get endpoint, headers: authorization_headers(current_user)
        expect(subject.size).to eq(expected_count)
      end
    end

    context 'when realtor' do
      let(:current_user) { create(:user, :realtor) }
      let(:data) { create_list(:user, 3) }
      let(:expected_status) { 403 }

      before { data }

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end
    end

    context 'when client' do
      let(:current_user) { create(:user, :client) }
      let(:data) { create_list(:user, 3) }
      let(:expected_status) { 403 }

      before { data }

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end
    end
  end
end
