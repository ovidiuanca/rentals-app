# frozen_string_literal: true

require 'rails_helper'

describe 'PUT /users/:id', type: :request do
  let(:endpoint) { "/users/#{user.id}" }

  let(:body) do
    {
      data: {
        id: user.id,
        attributes: {
          email: 'updated@example.com'
        },
        type: 'users'
      }
    }
  end

  context 'unauthenticated' do
    let(:current_user) { nil }
    let!(:user) { create(:user) }
    let(:expected_status) { 401 }

    it 'returns expected status' do
      put endpoint, headers: authorization_headers(current_user), params: body.to_json
      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    context 'when admin' do
      let!(:current_user) { create(:user, :admin) }
      let!(:user) { create(:user) }
      let(:expected_status) { 200 }
      let(:expected_email) { 'updated@example.com' }

      it 'returns expected status' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end

      it 'updates user' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(user.reload.email).to eq(expected_email)
      end

      it 'allows updating password' do
        body[:data][:attributes][:password] = 'updatedpass'

        put endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end

      it 'allows updating role' do
        body[:data][:attributes][:role] = 'realtor'

        put endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end
    end

    context 'when realtor' do
      let!(:current_user) { create(:user, :realtor) }
      let!(:user) { create(:user) }
      let(:expected_status) { 400 }

      it 'returns expected status' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end
    end

    context 'when client' do
      let!(:current_user) { create(:user, :client) }
      let!(:user) { create(:user) }
      let(:expected_status) { 400 }

      it 'returns expected status' do
        put endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end
    end
  end
end
