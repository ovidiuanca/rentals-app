# frozen_string_literal: true

require 'rails_helper'

describe 'POST /users', type: :request do
  let(:endpoint) { '/users' }

  let(:body) do
    {
      data: {
        type: 'users',
        attributes: {
          email: 'test@example.com',
          password: '12345678',
          'password-confirmation': '12345678'
        }
      }
    }
  end

  context 'unauthenticated' do
    let(:current_user) { nil }
    let(:expected_status) { 201 }

    it 'returns expected status' do
      post endpoint, headers: authorization_headers(current_user), params: body.to_json

      expect(response.status).to eq(expected_status)
    end

    it 'creates proper user' do
      post endpoint, headers: authorization_headers(current_user), params: body.to_json

      expect(User.last.email).to eq('test@example.com')
    end
  end

  context 'authenticated' do
    subject { JSON.parse(response.body)['data'] }

    context 'when admin' do
      let(:current_user) { create(:user, :admin) }
      let(:expected_status) { 201 }

      it 'returns expected status' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end

      it 'creates proper user' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(User.last.email).to eq('test@example.com')
      end

      it 'can create user with role' do
        body[:data][:attributes][:role] = 'realtor'

        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(201)
        expect(User.last.email).to eq('test@example.com')
      end
    end

    context 'when realtor' do
      let(:current_user) { create(:user, :realtor) }
      let(:expected_status) { 201 }

      it 'returns expected status' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end

      it 'creates proper user' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(User.last.email).to eq('test@example.com')
      end

      it 'cannot create user with role' do
        body[:data][:attributes][:role] = 'realtor'

        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(400)
      end
    end

    context 'when client' do
      let(:current_user) { create(:user, :client) }
      let(:expected_status) { 201 }

      it 'returns expected status' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(expected_status)
      end

      it 'creates proper user' do
        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(User.last.email).to eq('test@example.com')
      end

      it 'cannot create user with role' do
        body[:data][:attributes][:role] = 'realtor'

        post endpoint, headers: authorization_headers(current_user), params: body.to_json

        expect(response.status).to eq(400)
      end
    end
  end
end
