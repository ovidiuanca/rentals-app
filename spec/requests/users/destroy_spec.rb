# frozen_string_literal: true

require 'rails_helper'

describe 'DELETE /users/:id', type: :request do
  let(:endpoint) { "/users/#{user.id}" }
  let!(:user) { create(:user) }

  context 'unauthenticated' do
    let(:current_user) { nil }
    let(:expected_status) { 401 }

    it 'returns expected status' do
      delete endpoint, headers: authorization_headers(current_user)
      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    subject { JSON.parse(response.body)['data'] }

    context 'when admin' do
      let!(:current_user) { create(:user, :admin) }
      let(:expected_status) { 204 }

      it 'returns expected status' do
        delete endpoint, headers: authorization_headers(current_user)
        puts response.body

        expect(response.status).to eq(expected_status)
      end

      it 'deletes user' do
        expect do
          delete endpoint, headers: authorization_headers(current_user)
        end.to change { User.count }.from(2).to(1)
      end
    end

    context 'when realtor' do
      let!(:current_user) { create(:user, :realtor) }
      let(:expected_status) { 403 }

      it 'returns expected status' do
        delete endpoint, headers: authorization_headers(current_user)

        expect(response.status).to eq(expected_status)
      end

      it 'does not delete user' do
        expect do
          delete endpoint, headers: authorization_headers(current_user)
        end.not_to(change { User.count })
      end
    end

    context 'when client' do
      let!(:current_user) { create(:user, :client) }
      let(:expected_status) { 403 }

      it 'returns expected status' do
        delete endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end

      it 'does not delete user' do
        expect do
          delete endpoint, headers: authorization_headers(current_user)
        end.not_to(change { User.count })
      end
    end
  end
end
