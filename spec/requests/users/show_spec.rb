# frozen_string_literal: true

require 'rails_helper'

describe 'GET /users/:id', type: :request do
  let(:endpoint) { "/users/#{user.id}" }

  context 'unauthenticated' do
    let!(:current_user) { nil }
    let!(:user) { create(:user) }
    let(:expected_status) { 401 }

    it 'returns expected status' do
      get endpoint, headers: authorization_headers(current_user)
      expect(response.status).to eq(expected_status)
    end
  end

  context 'authenticated' do
    subject { JSON.parse(response.body)['data'] }

    context 'when admin' do
      let!(:current_user) { create(:user, :admin) }
      let!(:user) { create(:user) }
      let(:expected_status) { 200 }

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end

      it 'returns expected user' do
        get endpoint, headers: authorization_headers(current_user)
        expect(subject['id']).to eq(user.id.to_s)
      end
    end

    context 'when realtor' do
      let!(:current_user) { create(:user, :realtor) }
      let!(:user) { create(:user) }
      let(:expected_status) { 403 }

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end
    end

    context 'when client' do
      let!(:current_user) { create(:user, :client) }
      let!(:user) { create(:user) }
      let(:expected_status) { 403 }

      it 'returns expected status' do
        get endpoint, headers: authorization_headers(current_user)
        expect(response.status).to eq(expected_status)
      end
    end
  end
end
