# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence :email do |n|
      "test#{n}@example.com"
    end
    password { '12345678' }
    password_confirmation { '12345678' }
    role { 'client' }

    trait :client do
      role { 'client' }
    end

    trait :realtor do
      role { 'realtor' }
    end

    trait :admin do
      role { 'admin' }
    end
  end
end
