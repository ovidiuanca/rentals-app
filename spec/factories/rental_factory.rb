# frozen_string_literal: true

FactoryBot.define do
  factory :rental do
    name { 'rental name' }
    description { 'rental description' }
    price_per_month { 400 }
    floor_area_size { 1200 }
    number_of_rooms { 3 }
    lat { 46.767440 }
    long { 23.598508 }
    is_available { true }
    association :user, factory: :user

    trait :not_available do
      is_available { false }
    end
  end
end
